package kr.mohi.rpgcore.skill;

import java.util.LinkedHashMap;
import java.util.Map;

import cn.nukkit.Server;

public class Skill implements ISkill {
	/**
	 * Every skill extends this.
	 */
	protected static Skill instance = new Skill();
	protected Integer level = new Integer(0); // Skill level 0 means
												// disabled-skill. You can't use
												// this skill if the level is 0.
	protected Integer id;
	protected static Map<Integer, Skill> skills = new LinkedHashMap<Integer, Skill>();
	protected Integer maxLevel = new Integer(5);
	protected String name = null;

	public static final int SUCCESS_LEVEL_UP = 5;
	public static final int Fail_MAX_LEVEL = 6;

	private Skill() {
		// Skill is singleton.
	}

	public static Skill getInstance() {
		if (Skill.instance == null) {
			return new Skill();
		}
		return Skill.instance;
	}

	public void init() {

	}

	public static Map<Integer, Skill> getSkills() {
		return Skill.skills;
	}

	public String getName() {
		return this.name;
	}

	public Server getServer() {
		return Server.getInstance();
	}

	public Integer getLevelLimit() {
		return this.maxLevel;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(int n) {
		if (n > this.maxLevel)
			return;
		this.level = n;
	}

	public int addLevel() {
		if (this.maxLevel < this.level) {
			this.level++;
			return Skill.SUCCESS_LEVEL_UP;
		} else {
			return Skill.Fail_MAX_LEVEL;
		}
		// 에러 코드 반환, 예외처리 하세요
	}

	public static void registerSkill(Skill skill) {
		if (Skill.skills.containsKey(skill.getID()))
			return; // 스킬 아이디가 같으면 무시
		Skill.skills.put(skill.getID(), skill);
	}

	public Integer getID() {
		return this.id;
	}

	@Override
	public Skill clone() {
		return new Skill();
	}
}
