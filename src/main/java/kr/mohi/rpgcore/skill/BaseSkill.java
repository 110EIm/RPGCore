package kr.mohi.rpgcore.skill;

import cn.nukkit.Server;

public class BaseSkill implements ISkill {
	private Skill instance = Skill.getInstance().clone();
	static {
		Skill.getInstance().level = 0;
		Skill.getInstance().maxLevel = 5;
	}

	public String getName() {
		return instance.getName();
	}

	public Integer getID() {
		return instance.getID();
	}

	public Integer getLevelLimit() {
		return this.instance.getLevelLimit();
	}

	public Integer getLevel() {
		return this.instance.getLevel();
	}

	public Server getServer() {
		return this.instance.getServer();
	}

	/**
	 * You can Override this method. <br>
	 * It called when Skill was registered.
	 * 
	 * @see RPGCore
	 * @param level
	 * @param maxLevel
	 */
	public void init() {
		Skill.registerSkill(this.instance);
	}
}
