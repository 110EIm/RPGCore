package kr.mohi.rpgcore.skill;

import cn.nukkit.Server;

public interface ISkill extends Cloneable {
	
	/**
	 * Returns this skill name.
	 * 
	 * @return This skill name.
	 */
	String getName();
	
	/**
	 * Returns this skill id.
	 * 
	 * @return Skill id
	 */
	Integer getID();
	
	/**
	 * Returns level limit of this skill.
	 * 
	 * @return Level limit of this skill.
	 */
	Integer getLevelLimit();
	
	/**
	 * Returns enhancement of this skill.
	 * 
	 * @return Enhancement of this skill.
	 */
	Integer getLevel();
	
	/**
	 * Returns server
	 * @return {@code Server}
	 */
	Server getServer();
	
	/**
	 * initialize this skill.
	 */
	void init();
	
}
